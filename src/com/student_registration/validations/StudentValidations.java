package com.student_registration.validations;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.student_registration.models.StudentModel;

/**
 * @author Group-B
 *
 */

public class StudentValidations {
	public static  Pattern regexPattern;
	static Matcher regMatcher;
	static final String STRING_REGEX = "^[a-zA-Z\\s]+$";
	static StudentModel student = new StudentModel();
	public static final int SYSTEM_YEAR = Calendar.getInstance().get(Calendar.YEAR);
	
	
	

	
	/**
	 * Method for validating the String: Removing the front and back spaces by using
	 * trim method and checking with String (Regex) pattern. If input String matches
	 * with specific pattern it returns true else returns false.
	 */
	public static boolean stringValidation(String isValid) {

		if (isValid.trim().matches(STRING_REGEX)) {

			return true;
		} else {
			System.out.println("Please enter the valid String");
			return false;
		}
	}

	/**
	 * Method validating studentEmail
	 * 
	 * @param studentEmail
	 * @return boolean
	 */
	public static boolean emailValidation(String studentEmail) {
		regexPattern = Pattern.compile("^[(a-zA-Z-0-9-\\_\\+\\.)]+@[(a-z-A-z)]+\\.[(a-zA-z)]{2,3}$");
		regMatcher = regexPattern.matcher(studentEmail);
		if (regMatcher.matches()) {
			return true;
		} else {
			System.out.println("Please enter the valid emialId");
			return false;
		}

	}
	/**
	 * Method validating student Joining year
	 * 
	 * @param joiningYear
	 * @return boolean
	 */
	public static  boolean joiningYear(String joiningYear) {
		int joiningYear1 = Integer.parseInt(joiningYear);
		if (joiningYear1 > 2010 && joiningYear1 <= SYSTEM_YEAR) {
			return true;
		} else {
			System.out.println("Please enter the valid joining  year");
			return false;
		}

	}

	/**
	 * Method to validate address of student
	 * 
	 * @param studentAddress
	 * @return boolean
	 */
	public static boolean addressValidation(String studentAddress) {
		if (studentAddress.matches("\\d+\\s+([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+)")) {
			return true;

		} else {
			return true;
		}
	}


	/**
	 * method to validate mobile number of student
	 * 
	 * @param studentMobileno
	 * @return boolean
	 */
	public static boolean mobileNumberValidation(String studentMobileno) {
		regexPattern = Pattern.compile("^\\+[0-9]{2,3}+-[0-9]{10}$");
		regMatcher = regexPattern.matcher(studentMobileno);
		if (regMatcher.matches()) {
			return true;

		} else {
			System.out.println("Please enter the valid Mobile no.");
			return false;
		}
	}

	/**
	 * Method to validate Date Of Birth of student
	 * 
	 * @param studentDob
	 * @return boolean
	 */
	public static boolean dateOfBirthValidation(String studentDob) {

		String date1 = "^(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[012])-([12][0-9]{3})$";
		if (studentDob.matches(date1)) {
			int dobYear = Integer.parseInt((studentDob.split("-"))[2]);
            if(SYSTEM_YEAR - dobYear > 15  )
            	return true;
            else {
            	System.out.println("Your age should be grater than 15");
            	return false;
            }
		} else {
			System.out.println("Please enter the valid dob");
			return false;
		}
	}
	/**
	 * This method checks the qualification is either inter or deploma
	 * @param studentQualification
	 * @return
	 */
	public static boolean qualificationValidation(String studentQualification) {
		if((studentQualification.equalsIgnoreCase("Inter") || (studentQualification.equalsIgnoreCase("Diploma")))) {
			return true;
		}
		else {
			System.out.println("qualification should be Inter/Diploma");
			return false;
		}
		
	}
	
}
