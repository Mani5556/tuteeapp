package com.student_registration.models;

import java.io.Serializable;

public class StudentMarks implements Serializable {
	private static final long serialVersionUID = 6529685098267757690L;
	private int subject1;
	private int subject2;
	private int subject3;
	private int subject4;
	private int subject5;
	private int subject6;
	private int semBacklogs;
	public int getSemBacklogs() {
		return semBacklogs;
	}
	public void setSemBacklogs(int semBacklogs) {
		this.semBacklogs = semBacklogs;
	}
	/**
	 * @return the subject1
	 */
	public int getSubject1() {
		return subject1;
	}
	/**
	 * @param subject1 the subject1 to set
	 */
	public void setSubject1(int subject1) {
		this.subject1 = subject1;
	}
	/**
	 * @return the subject2
	 */
	public int getSubject2() {
		return subject2;
	}
	/**
	 * @param subject2 the subject2 to set
	 */
	public void setSubject2(int subject2) {
		this.subject2 = subject2;
	}
	/**
	 * @return the subject3
	 */
	public int getSubject3() {
		return subject3;
	}
	/**
	 * @param subject3 the subject3 to set
	 */
	public void setSubject3(int subject3) {
		this.subject3 = subject3;
	}
	/**
	 * @return the subject4
	 */
	public int getSubject4() {
		return subject4;
	}
	/**
	 * @param subject4 the subject4 to set
	 */
	public void setSubject4(int subject4) {
		this.subject4 = subject4;
	}
	/**
	 * @return the subject5
	 */
	public int getSubject5() {
		return subject5;
	}
	/**
	 * @param subject5 the subject5 to set
	 */
	public void setSubject5(int subject5) {
		this.subject5 = subject5;
	}
	/**
	 * @return the subject6
	 */
	public int getSubject6() {
		return subject6;
	}
	/**
	 * @param subject6 the subject6 to set
	 */
	public void setSubject6(int subject6) {
		this.subject6 = subject6;
	}
	

}
