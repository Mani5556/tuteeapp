package com.student_registration.models;

import java.io.Serializable;

public class UtilityClass implements Serializable {
	private static final long serialVersionUID = 6529685098267757690L;
	private int lastStuddentId;

	public int getLastStuddentId() {
		return lastStuddentId;
	}

	public void setLastStuddentId(int lastStuddentId) {
		this.lastStuddentId = lastStuddentId;
	}

}
