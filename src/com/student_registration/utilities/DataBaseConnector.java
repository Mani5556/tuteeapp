package com.student_registration.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseConnector {
		public static Connection getConnection() {
			Connection conn = null;
			try {
				Class.forName("com.mysql.jdbc.Driver");
			    conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/studentregistrationapp", "root", "innominds");
			} 
			catch (SQLException e) {
					e.printStackTrace();
			}
		     catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			return conn;
		
		}
		
}
