package com.student_registration.controllers;

import com.student_registration.enums.EnumClasses;
import com.student_registration.models.AdminModel;
import com.student_registration.models.StudentModel;
import com.student_registration.services.AdminService;
import com.student_registration.services.SearchStudent;
import com.student_registration.services.StudentService;
import com.student_registration.utilities.CommonClass;
import com.student_registration.validations.AdminValidations;
import java.util.*;

/**
 * this class is for creating the Home page it handles create admin and register
 * new admin
 * 
 * @author Group B
 *
 */
public class AdminPage {
	AdminPage adminPageObj;
	CommonClass common;
	AdminValidations adminValidation;
	AdminService adminService;
	StudentService studentServiece;

	public AdminPage() {
		adminPageObj = this;
		common = new CommonClass();
		adminValidation = new AdminValidations();
		adminService = new AdminService();
		studentServiece = new StudentService();
	}

	/**
	 * this method shows the Home page for the admin having 1.register as Admin
	 * 2.login as admin options
	 */
	public void adminHomePage() {
		Scanner scan = new Scanner(System.in);
		// this method show the registration form for the admin
		outerWhileLoop: while (true) {
			int adminChoice = -1;
			innerWhileLoop: while (true) {
				System.out.println(
						"                                Admin Home Page   \n                          -----------------------------");
				System.out.println("Select your Option:");
				System.out.println("------------------");
				System.out.println("1.Register as admin\n2.Login as Admin");
				try {
					adminChoice = Integer.parseInt(scan.next());
					break innerWhileLoop;
				} catch (Exception ime) {
					System.out.println("Please Enter Only Integer Values...!!!");

				}
			}
			switch (adminChoice) {
			case 1:
				adminPageObj.showAdminRegistrationForm();
				break outerWhileLoop;
			case 2:
				int AdminLoginAttempts = 0;
				// this call shows the admin Login page to the user
				common.showLoginForm(AdminLoginAttempts, "Admin");
				break outerWhileLoop;
			default:
				System.out.println("Choose Correct Option");
				break;
			}
		}
		scan.close();
	}

	/**
	 * this method shows the adminRegistration form
	 * 
	 * @param Scnner ,AdminModel,AdminPageObject
	 */

	public void showAdminRegistrationForm() {
		System.out.println(
				"                                   Admin Registration Form \n                          --------------------------------------------------------");
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		AdminModel admin = new AdminModel();
		boolean callAgain = true;
		while (callAgain) {
			try {
				System.out.print("Enter Unique ID:");
				String uniqueId = scan.next();
				admin.setUniqueId(uniqueId);
				callAgain = false;
				break;
			} catch (Exception ime) {
				System.out.println("Please Enter Only Integer Values...!!!");
			}
		}

		// Asking for his UserName
		System.out.print("Enter Admin UserName:");
		String newAdminUserName = scan.next();
		boolean isValidUserName = false;
		while (!isValidUserName) {
			isValidUserName = adminValidation.adminUserNameValidation(newAdminUserName);
			if (isValidUserName == true) {
				break;
			}
			System.out.println("Invalid Username..Please Enter Valid Name!!!");
			System.out.print("Enter Admin UserName:");
			newAdminUserName = scan.next();
		}
		admin.setAdminUserName(newAdminUserName);

		// Asking for Password to secure his account
		System.out.print("Enter Password:");
		String newAdminPassword = scan.next();
		boolean isValidPassword = false;
		while (!isValidPassword) {
			isValidPassword = AdminValidations.adminPasswordValidate(newAdminPassword);
			if (isValidPassword == true) {
				break;
			}
			System.out.println(
					"Invalid Password!!!Password should contain atleast one UpperCase,LowerCase&Special characters of size 8");
			System.out.print("Enter Password:");
			newAdminPassword = scan.next();
		}
		admin.setAdminPassword(newAdminPassword);

		// Asking admin to re-enter the password
		System.out.print("Re-Enter Password:");
		String adminReEnterPassword = scan.next();
		admin.setAdminReEnterPassword(adminReEnterPassword);

		// asking the security question for forgot password purpose
		System.out.println("Security Question :::");
		System.out.print("Enter the Favorite place:");
		String adminSecurityQuestion = scan.next();
		admin.setAdminSecurityQue(adminSecurityQuestion);
		/*
		 * Iterating the While Loop to check whether both password fields are correct or
		 * not..If both are not matches we again asks admin to re-enter the password
		 */
		while (!(admin.getAdminPassword().equals(admin.getAdminReEnterPassword()))) {
			System.out.println("Please Enter Correct Password");
			System.out.print("Re-Enter Password:");
			String adminEnterPassword = scan.next();
			admin.setAdminReEnterPassword(adminEnterPassword);
		}

		// submitting the admin object to the file
		try {
			adminService.signUpAdmin(admin);
			System.out.println("Admin register successfully");
			System.out.println("_________________________________");
			adminPageObj.adminHomePage();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("_________________________________");
			adminPageObj.showAdminRegistrationForm();
		}
	}

	/**
	 * this method shows the admin functionalites to the user
	 * 
	 * @param AdminModel
	 */
	public void adminsOperations(AdminModel admin) {
		Scanner scan = new Scanner(System.in);
		System.out.println("                                    Welcome " + admin.getAdminUserName());
		System.out.println("                              ------------------------------------------------");
		StudentPage studentPage = new StudentPage();
		outerWhileLoop: while (true) {
			int choice = -1;
			whileloop: while (true) {
				System.out.println(
						"Choose The Operations You Want to Perform:\n-----------------------------------------");
				System.out.println(
						"1.Register New Student\n2.Update Student Details\n3.Search Student\n4.Delete Student Details\n5.View StudentMarks\n6.Student Fee Detais\n7.Logout");
				try {
					choice = Integer.parseInt(scan.next());
					break whileloop;
				} catch (Exception e) {
					System.out.println("Please Choose Only Integers.....!!!");
				}
			}
			switch (choice) {
			case 1:
				if (studentPage.showStudentRegisterForm()) {
					System.out.println("Student registered  successfully");
					System.out
							.println("------------------------------------------------------------------------------");
				} else {
					System.out.println("Student registered  failed");
					System.out.println(
							"---------------------------------------------------------------------------------");
				}
				break;
			case 2:
				new UpdateStudent().showUpdateForm();
				break;
			case 3:
				adminPageObj.showSearchOptions("searchBy::",null);
				break;
			case 4:
				System.out.println("please enter the studentId you want to delete ");
				String studentId = scan.next();
				if (studentServiece.deleteStudent(studentId)) {
					System.out.println("Student deleted successfully");
					System.out.println("---------------------------------");
				} else {
					System.out.println("Oops some thing went wrong ...");
					System.out.println("---------------------------------");
				}
				break;
			case 5:
				System.out.println("please enter the StudentId ::");
				String id = scan.next();
				new CommonClass().showMarks(id);
				break;
			case 6:
				System.out.println("please enter the StudentId ::");
				String studentId1 = scan.next();
				new CommonClass().viewFeeDetails(studentId1);
				break;
			case 7:
				new HomePage().showHomePage();
				break outerWhileLoop;
				
			default:
				System.out.println("Please Choose correct option from 1 --> 7");
				System.out.println(
						"--------------------------------------------------------------------------------------------------");
			}

		}
		scan.close();
	}

	/**
	 * this method shows the search student options to the admin
	 * 
	 */

	private void showSearchOptions(String context,ArrayList<StudentModel> searchedResult) {
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		// this object is created for using the all the search metods in that class
		SearchStudent searchObj = new SearchStudent();
		int choice;
		whileLoop: while (true) {
			
			System.out.println(context + "\n---------------");
			System.out.println(
					"1.studentId\n2.Student Name\n3.Student course\n4.Student due\n5.student cast\n6.student year\n7.student backlogs\n8.gender\n9.All ");
			try {
				choice = scan.nextInt();
				if (choice < 1 || choice > 9)
					throw new Exception();
				break whileLoop;

			} catch (Exception e) {
				System.out.println("please enter the integer values from 1..9 only");
				System.out.println();
			}
		}

		// this switch case is used for searching the student details based on filter if
		// data find shows the details
		StudentInputTaking inputReader = new StudentInputTaking();
		ArrayList<StudentModel> students = new ArrayList<StudentModel>();
		ArrayList<StudentModel> searchedList = new ArrayList<StudentModel>();
		
		switch (choice) {
		case 1:
			System.out.println("Enter student Id");
			String studentId = scan.next();
			StudentModel student = searchObj.searchById(studentId);
			if(student != null) {
				students.add(student);
			}
			showStudentDetails(students);
			break;
		case 2:
			System.out.println("Enter student name");
			 String studentName = inputReader.askingName("firstname/lastname");
			 students = searchObj.search(EnumClasses.SearchKeys.NAME, studentName,searchedList);
			 showStudentDetails(students);
			 filterationOnSearch(students);
			break;
		case 3:
			System.out.println("Enter student course");
			String studentCourse = inputReader.askingStudentCourse();
			students = searchObj.search(EnumClasses.SearchKeys.COURSE, studentCourse.toLowerCase(),searchedList);
			showStudentDetails(students);
			filterationOnSearch(students);
			break;
		case 4:
			students = searchObj.search(EnumClasses.SearchKeys.DUE, "",searchedList);
			showStudentDetails(students);
			filterationOnSearch(students);
            break;
		case 5:
			System.out.println("enter cast");
			students = searchObj.search(EnumClasses.SearchKeys.CAST,scan.next().toLowerCase(),searchedList);
			showStudentDetails(students);
			filterationOnSearch(students);
			break;
			
		case 6:
			Integer option = -1;
			whileLoop:while(true){
				System.out.println("enter year");
			    System.out.println("1.first year\n2.second year\n3.third year\n4.fourth year\n5.old students");
			    try {
			    	option =  scan.nextInt();
			    	if(option < 0 || option >  5)throw new Exception();
			    	break whileLoop;
				} catch (Exception e) {
					System.out.println("Please enter only integer values from 1 to 5 ");
				}
			}
		   
			students = searchObj.search(EnumClasses.SearchKeys.YEAR,option.toString(),searchedList);
			showStudentDetails(students);
			filterationOnSearch(students);
			break;
			
		case 7:
			students = searchObj.search(EnumClasses.SearchKeys.BACKLOGS,"",searchedList);
			showStudentDetails(students);
			filterationOnSearch(students);
			break;
		case 8:
			String gender = inputReader.askingGender();
			students = searchObj.search(EnumClasses.SearchKeys.GENDER, gender,searchedList);
			showStudentDetails(students);
			filterationOnSearch(students);
			break;
		case 9:
			students = searchObj.search(EnumClasses.SearchKeys.ALL,"",searchedList);
			showStudentDetails(students);
			filterationOnSearch(students);
			break;
		default:
			break;
		}
	}

	/**
	 * this method shows the filtaration options on searched result list
	 * 
	 * @params List<StudentModels>
	 * @return list
	 */

	private void filterationOnSearch(ArrayList<StudentModel> searchedResult) {
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		if(searchedResult.size() > 1) {
		    while(true) {
		    	System.out.println("Do you want to filter \n------------------------\n1.yes\n2.No");
		    	try {
		    	     int choice = Integer.parseInt(scan.next());
		    	     if (choice > 2 || choice < 0)throw new Exception();
		    	     
		    	     if(choice == 1) {
		    	    	 adminPageObj.showSearchOptions("FilterBy:::",searchedResult);
		    	     }
		    	     break;
		    	}
		    	catch (Exception e) {
					System.out.println("enter only integer values 1 or 2");
				}
		    }
	   }
	
	}
	/**
	 * this method shows the student details in the tabular format
	 * 
	 * @param StudentModel student objs
	 * 
	 */

//
	public static void showStudentDetails(ArrayList<StudentModel> students) {
		
		if (students.size()> 0 ) {
			System.out.println("                                                             Student Details ");
			System.out.println(students.size() + " records found");
			System.out.println("====================================================================================================================================================================================================");
			System.out.println(
					"Student-Id   FirstName  LastName     MobileNo       Email                   DateOfBirth      Gender      Address   Course    Qualification   Joining Year  Caste   paid_Fee   due    backLogs");
			System.out.println("=====================================================================================================================================================================================================");
	
			for (StudentModel student : students) {
				System.out.println(student.getStudentId() + "   " + student.getFirstName() + "    " + student.getLastname()
						+ "         " + student.getMobileNo() + "   " + student.getEmail() + "      " + student.getDateOfBirth() + "       "
						+ student.getGender() + "       " + student.getAddress() + "    " + student.getCourse() + "    " + "   " + student.getQualification() + "   "
						+ "           " + student.getJoiningYear() + "     "+student.getCaste() + "    "+ student.getFees() + "   " + student.getDue() + "     " + student.getBacklogs());
				System.out.println("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			}
			
		} else {
			System.out.println("No students found with this data ");
			System.out.println("____________________________________________________________________________________");
		}
	}

}
