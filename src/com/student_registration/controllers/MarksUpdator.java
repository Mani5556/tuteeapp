package com.student_registration.controllers;

import java.util.Calendar;
import java.util.Scanner;
import java.util.TreeMap;

import com.student_registration.models.StudentMarks;
import com.student_registration.models.StudentModel;
import com.student_registration.services.StudentService;

public class MarksUpdator {
	public static final int PASSMARK = 35;
	public static final int PRESENT_YEAR = Calendar.getInstance().get(Calendar.YEAR);
	int backLogs;
	int semBacklogs;

	/**
	 * This method update the marks and stores the data into the given student
	 * object
	 * 
	 * @param student
	 * @param scan
	 * @return updated student object
	 */
	public TreeMap<String, StudentMarks> updateMarks(StudentModel student) {
		backLogs = student.getBacklogs();
		
		int currentCourseYear = student.getPusrsuingYear();
		StudentService studentService = new StudentService();
		TreeMap<String, StudentMarks> marks ;
		/*
		 * if condition to check TreeMap object is empty or not if empty creates new
		 * object if not it gets the data of marks
		 */
		marks = studentService.readMarks(student.getStudentId());
		if (currentCourseYear > 4) {
			currentCourseYear = 4;
		}
		int i;
        if(student.getQualification().equalsIgnoreCase("inter")){
        	i = marks.size() + 1;
        }
        else
        	i = marks.size() + 3;
        
        if(i > currentCourseYear * 2) {
        	System.out.println("marks already updated u can view the marks ");
        	return marks;
        	
        }
        
		for (i = marks.size() + 1; i <= currentCourseYear * 2; i++) {
			System.out.println("Enter the Marks of semester:" + i);
			StudentMarks studentMarks = new StudentMarks();
			 semBacklogs = 0;

			// taking subject 1 marks and checking with passmark and in the range of >0 and
			// <=100
			// reading the 6 subject marks
			studentMarks.setSubject1(readMarks( 1));
			studentMarks.setSubject2(readMarks( 2));
			studentMarks.setSubject3(readMarks( 3));
			studentMarks.setSubject4(readMarks( 4));
			studentMarks.setSubject5(readMarks( 5));
			studentMarks.setSubject6(readMarks( 6));
			studentMarks.setSemBacklogs(semBacklogs);
            marks.put("sem"+i, studentMarks);
		}
		
		student.setMarks(marks);
		student.setBacklogs(backLogs);
        return marks;
	}

	/**
	 * this method asking the user to read the marks from the keybaord
	 * 
	 * @return marks
	 */
	int readMarks( int subjectNumber) {
		int subjectMarks;
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);

		do {
			System.out.println("Enter the Subject" + subjectNumber + " Marks:");
			while(true) {
				try {
				     subjectMarks = Integer.parseInt(scan.next());
				     break;
				}catch (Exception e) {
					System.out.println("please enter only integer value ");
				}
			}
		
			if (subjectMarks < PASSMARK) {
				semBacklogs ++;
				backLogs++;
			}
			if (subjectMarks < 0 && subjectMarks > 100) {
				System.out.println("Please enter the marks from 0 .. 100 only");
			}
		} while (subjectMarks < 0 && subjectMarks > 100);
		return subjectMarks;
	}
}
