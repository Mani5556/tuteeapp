package com.student_registration.controllers;

import com.student_registration.models.*;
import com.student_registration.services.SearchStudent;
import com.student_registration.services.StudentService;

import java.util.*;

/**
 * This class will Update the Existing details of a Student an stores the
 * updated details in a file
 * 
 * @author Group B
 *
 */
public class UpdateStudent {

	StudentPage studentPage ;
	StudentService studentService;
    
	public UpdateStudent() {
		studentPage = new StudentPage();
		studentService = new StudentService();
	}

	/**
	 * This Method will allow the admin to update the student details
	 */
	public void showUpdateForm() {
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		System.out.println("-----------------------------------------------------");
		System.out.println("Enter the Student ID you want to Update:");
		String updateStudent = scan.next();
		SearchStudent searchObj = new SearchStudent();
		StudentModel student = searchObj.searchById(updateStudent);
		if (student != null) {
			System.out.println(student);
		} else {
			System.out.println("No student found");
		
			//scan.close();
			return;
		}
		System.out.println("Choose the Field which you want to Update");
		System.out.println("--------------------------------------------------------------------------------------------");
		System.out.println("1.First Name      2.Last Name    3.Date Of Birth   4.Mobile Number   5.Email ID");
		System.out.println("6.Address         7.Caste        8.Qualification   9.Course          10.Fees      ");
		System.out.println("11.Gender         12.AddMarks    13.Exit");
		System.out.println("---------------------------------------------------------------------------------------------");
		StudentInputTaking inputTaking = new StudentInputTaking();
		

		whileloop: while (true) {
			int choice = -1;
			innerWhile: while(true) {
				System.out.println("Enter your Choice:");
				try {
				choice = scan.nextInt();
				break innerWhile;
				}catch (Exception e) {
					System.out.println("please enter the Integer values only");
				}
			}

			switch (choice) {
			// case 1: is for updating first name
			case 1:
				String updateFirstName = inputTaking.askingName("firstName");
				student.setFirstName(updateFirstName);
                updateStatusInfo(studentService.updateStudent(student.getStudentId(), "firstName", updateFirstName));
				break;
			// case 2: is for updating Last name
			case 2:
				String updateLastName = inputTaking.askingName("lastName");
				student.setLastname(updateLastName);
				 updateStatusInfo(studentService.updateStudent(student.getStudentId(), "lastName", updateLastName));
				break;
			// case 3: is for updating Date Of Birth
			case 3:
				String studentDob = inputTaking.askingDateOfBirth();
				student.setDateOfBirth(studentDob);
				updateStatusInfo(studentService.updateStudent(student.getStudentId(), "dob", studentDob));
				break;
			// case 4: is for updating Mobile Number
			case 4:

				String studentMobileno = inputTaking.askingMobileNumber();
				student.setMobileNo(studentMobileno);
				updateStatusInfo(studentService.updateStudent(student.getStudentId(), "mobileNo", studentMobileno));
				break;
			// case 5: is for updating Email Id
			case 5:
				String studentEmail = inputTaking.askingEmailID();
				student.setEmail(studentEmail);
				updateStatusInfo(studentService.updateStudent(student.getStudentId(), "email", studentEmail));
				break;
			// case 6: is for updating Address
			case 6:
				String studentAddress = inputTaking.askingStudentAddress();
				student.setAddress(studentAddress);
				updateStatusInfo(studentService.updateStudent(student.getStudentId(), "address", studentAddress));
				break;

			// case 7: is for updating Caste
			case 7:
				String caste = inputTaking.askingCaste();
				student.setCaste(caste);
				updateStatusInfo(studentService.updateStudent(student.getStudentId(), "caste", caste));
				break;

			// case 8: is for updating Qualification
			case 8:
				String studentQualification = inputTaking.askingStudentQualification();
				student.setQualification(studentQualification);
				updateStatusInfo(studentService.updateStudent(student.getStudentId(), "qualification", studentQualification));
				break;

			// case 9: is for updating Course
			case 9:
				String studentCourse = inputTaking.askingStudentCourse();
				student.setCourse(studentCourse);
				updateStatusInfo(studentService.updateStudent(student.getStudentId(), "course", studentCourse));
				break;

			// case 10: is for updating fee
			case 10:
				double paidFees = inputTaking.askingFeesPaid();
				student.calculateDueAndSet(student.getCaste(), paidFees);
				updateStatusInfo(studentService.updateFees(student.getStudentId(), paidFees));
				break;
			// case 11: is for updating Gender
			case 11:
				String gender = inputTaking.askingGender();
				student.setGender(gender);
				updateStatusInfo(studentService.updateStudent(student.getStudentId(), "gender", gender));
				break;

			case 12:
				TreeMap<String,StudentMarks> marks = new MarksUpdator().updateMarks(student);
				
				if(marks.isEmpty()) {
					System.out.println("marks already updated");
				}else {
				  if( studentService.updateMarks(marks, student.getStudentId()) > 0) {
					  updateStatusInfo(true);
				  }
				  else
					  updateStatusInfo(true);
				}
				break ;

			case 13:
				break whileloop;
			
			default:
				 System.out.println("Please enter the valid input from 1.. 13");
				break;
			}
			
		}
  
	}

	/**
	 * this method updates the student details
	 * 
	 * @param StudentModel
	 */

	void updateStatusInfo(boolean updateStatus) {
		if (updateStatus) {
			System.out.println("Updation is Succesful");
		} else {
			System.out.println("Updation is Failed");
		}
	}
}
