package com.student_registration.services;
import java.util.TreeMap;
import com.student_registration.dao.AdminDbConnection;
import com.student_registration.models.AdminModel;

/**
 * this class handles the all the logics on admin module 
 * @author GROUP-B
 *
 */
public class AdminService {
    TreeMap<Integer,AdminModel> adminList;
	AdminDbConnection adminDbConnection = new AdminDbConnection();
	
    
	/**
	 * this method calls the sigUp method of AdminDBconnection class ...
	 * 
	 * @param admin is AdminModel type
	 * @return true if admin registers successfully else false
	 */
	
	public  void signUpAdmin(AdminModel admin) throws Exception {
		 adminDbConnection.signUpAdmin(admin);	
	}
	
	/**
	 * this method calls the adminLogin method of adminDbConnection class 
	 * 
	 * @param adminInfo
	 * @return true if the admin is Valid admin else return false
	 */

	public AdminModel adminLogin(String username,String password) {
		return  adminDbConnection.adminLogin(username, password);	
	}
	

	/**
	 * this method searching the admin by the username and security code returns the
	 * key of the admin
	 * 
	 * @param adminForgotDetails
	 * @return admin index in treeMap of admins
	 */

	public String adminForgotPassWord(String userName,String securityQue) {
		return  adminDbConnection.adminForgotPassWord(userName, securityQue);	
	}
	
	/**
	 * this method resets the admin password
	 * 
	 * @param object index or key of map ,new password details
	 * 
	 */
	public  boolean resetAdminPassword(String newPassWord, int searchKey) {
		return  adminDbConnection.resetAdminPassword(newPassWord, searchKey);		
	}
	
	


}
