package com.student_registration.services;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;
import java.util.TreeMap;
import com.student_registration.dao.SearchStudentDAO;
import com.student_registration.dao.StudentDbconnection;
import com.student_registration.enums.EnumClasses;
import com.student_registration.models.StudentMarks;
import com.student_registration.models.StudentModel;
import com.student_registration.utilities.DataBaseConnector;

/**
 * this class is having the all the different searching methods for the student
 * details
 */
public class SearchStudent {
	Scanner scanner = new Scanner(System.in);
	static int systemYear = Calendar.getInstance().get(Calendar.YEAR);
	StudentDbconnection studentDbconnection;
	public SearchStudent() {
		studentDbconnection = new StudentDbconnection();
	}




	public TreeMap<String, StudentMarks> readMarks(String studentId) {
		Connection con = DataBaseConnector.getConnection();
	    TreeMap<String, StudentMarks> marksList= new TreeMap<String,StudentMarks>();
		try {
		
            Statement statment = con.createStatement();
			ResultSet rs = statment.executeQuery("select * from student_marks  where studentId = "+studentId);
            
			while (rs.next()) {
			     StudentMarks marks = new  StudentMarks();
			     marks.setSubject1(rs.getInt(3));
			     marks.setSubject2(rs.getInt(4));
			     marks.setSubject3(rs.getInt(5));
			     marks.setSubject4(rs.getInt(6));
			     marks.setSubject5(rs.getInt(7));
			     marks.setSubject6(rs.getInt(8));
			     
			     marksList.put(rs.getString(2), marks);
			}
		} catch (Exception e) {	
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return marksList;
	
	}

  public ArrayList<StudentModel> search(EnumClasses.SearchKeys context, String input,ArrayList<StudentModel> searchedList) {
		SearchStudentDAO searchStudentDAO = new SearchStudentDAO();
		return searchStudentDAO.search(context, input, searchedList);
		
	} 

	/**
	 * this method searches the student details by Id
	 * 
	 * @param Sting Student Id
	 * @return StudentModel object
	 */

//SearchStudentDAO sd;
  public StudentModel searchById(String studentId) {
	  SearchStudentDAO searchStudentDAO = new SearchStudentDAO();
		return searchStudentDAO.searchById(studentId);
		
	}



}
