package com.student_registration.enums;

public class EnumClasses {
	public static enum SearchKeys{
		ALL,
		NAME,
		COURSE,
		DUE,
		CAST,
		YEAR,
		BACKLOGS,
		GENDER
	}
}
