-- MySQL dump 10.13  Distrib 5.7.24, for Win64 (x86_64)
--
-- Host: localhost    Database: studentregistrationapp
-- ------------------------------------------------------
-- Server version	5.7.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_table`
--

DROP TABLE IF EXISTS `admin_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_table` (
  `empId` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `securityQue` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`empId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_table`
--

LOCK TABLES `admin_table` WRITE;
/*!40000 ALTER TABLE `admin_table` DISABLE KEYS */;
INSERT INTO `admin_table` VALUES (1,'mani','Mani@123','myVillage'),(2,'krishna','Krishna@123','vijayawada'),(3,'raju','Raju@123','Vizag');
/*!40000 ALTER TABLE `admin_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_by_cast_table`
--

DROP TABLE IF EXISTS `fee_by_cast_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fee_by_cast_table` (
  `caste` varchar(45) NOT NULL,
  `fees` int(11) NOT NULL,
  PRIMARY KEY (`caste`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_by_cast_table`
--

LOCK TABLES `fee_by_cast_table` WRITE;
/*!40000 ALTER TABLE `fee_by_cast_table` DISABLE KEYS */;
INSERT INTO `fee_by_cast_table` VALUES ('BC',45000),('OC',50000),('SC',40000),('ST',35000);
/*!40000 ALTER TABLE `fee_by_cast_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_authentication_table`
--

DROP TABLE IF EXISTS `student_authentication_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_authentication_table` (
  `studentId` varchar(30) NOT NULL,
  `password` varchar(45) DEFAULT NULL,
  `security_que` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`studentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_authentication_table`
--

LOCK TABLES `student_authentication_table` WRITE;
/*!40000 ALTER TABLE `student_authentication_table` DISABLE KEYS */;
INSERT INTO `student_authentication_table` VALUES ('CN0010001','Inno@123','myCollage'),('CN0010002','Inno@123','myCollage'),('CN0010003','Inno@123','myCollage'),('CN0010004','Inno@123','myCollage'),('CN0010005','Inno@123','myCollage');
/*!40000 ALTER TABLE `student_authentication_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_fees_details`
--

DROP TABLE IF EXISTS `student_fees_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_fees_details` (
  `studentId` varchar(30) NOT NULL,
  `paid_date` datetime NOT NULL,
  `paid_fees` double DEFAULT NULL,
  KEY `studentId_idx` (`studentId`),
  CONSTRAINT `studentId` FOREIGN KEY (`studentId`) REFERENCES `student_table` (`studentId`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_fees_details`
--

LOCK TABLES `student_fees_details` WRITE;
/*!40000 ALTER TABLE `student_fees_details` DISABLE KEYS */;
INSERT INTO `student_fees_details` VALUES ('CN0010001','2019-01-04 16:14:44',12000),('CN0010002','2019-01-04 16:16:16',20000),('CN0010001','2019-01-04 18:25:32',5000),('CN0010003','2019-01-05 10:16:05',35000),('CN0010004','2019-01-05 10:17:02',25000),('CN0010005','2019-01-05 10:18:12',36000),('CN0010001','2019-01-05 17:45:15',35000),('CN0010001','2019-01-05 17:45:29',1000);
/*!40000 ALTER TABLE `student_fees_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_marks`
--

DROP TABLE IF EXISTS `student_marks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_marks` (
  `student_id` varchar(30) NOT NULL,
  `sem_id` varchar(45) NOT NULL,
  `subject1` int(11) DEFAULT NULL,
  `subject2` int(11) DEFAULT NULL,
  `subject3` int(11) DEFAULT NULL,
  `subject4` int(11) DEFAULT NULL,
  `subject5` int(11) DEFAULT NULL,
  `subject6` int(11) DEFAULT NULL,
  `sem_Wise_Backlogs` int(20) NOT NULL,
  PRIMARY KEY (`student_id`,`sem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_marks`
--

LOCK TABLES `student_marks` WRITE;
/*!40000 ALTER TABLE `student_marks` DISABLE KEYS */;
INSERT INTO `student_marks` VALUES ('CN0010001','sem1',23,45,67,78,89,0,2),('CN0010001','sem2',2,3,45,54,78,1,3),('CN0010001','sem3',23,43,56,87,89,0,2),('CN0010001','sem4',32,6,6578,87,8,98,3);
/*!40000 ALTER TABLE `student_marks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_table`
--

DROP TABLE IF EXISTS `student_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_table` (
  `studentId` varchar(30) NOT NULL,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `mobileNo` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `gender` varchar(45) NOT NULL,
  `qualification` varchar(45) NOT NULL,
  `caste` varchar(45) NOT NULL,
  `course` varchar(45) NOT NULL,
  `joiningYear` int(11) NOT NULL,
  `address` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`studentId`),
  KEY `caste_idx` (`caste`),
  CONSTRAINT `caste` FOREIGN KEY (`caste`) REFERENCES `fee_by_cast_table` (`caste`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_table`
--

LOCK TABLES `student_table` WRITE;
/*!40000 ALTER TABLE `student_table` DISABLE KEYS */;
INSERT INTO `student_table` VALUES ('CN0010001','Mani','kumar','12-2-1996','+91-7898678999','ef@gmail.com','male','inter','OC','CSE',2017,'iran'),('CN0010002','krishna','kapoor','12-3-1994','+91-8768999989','hs@gmail.com','male','inter','BC','CSE',2016,'mvp'),('CN0010003','krishna','james','10-09-1997','+91-0909090909','gm@gmail.com','female','inter','OC','CSE',2017,'AUS'),('CN0010004','krishna','rock','10-08-1997','+91-0987654321','james@gmail.com','male','inter','OC','CSE',2017,'US'),('CN0010005','krishna','raju','10-09-1997','+91-0987654321','ramesh@gmail.com','male','diploma','BC','MECH',2017,'amalapuram');
/*!40000 ALTER TABLE `student_table` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-07  9:59:44
